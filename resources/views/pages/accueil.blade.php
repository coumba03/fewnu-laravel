@extends('pages.app')


@section('content')

<div class="header-content">
    <div class="row">
        <div class="col-md-6 col-sm-12 col-xs-12">
            <p>Decouvrez votre suite de logiciel de gestion fewnu</p>
            <div>
                <p>Téléchargez sur pc</p>
            </div>
            <div>
                <img src="images/google.png" alt="" class="w-25">
                <img src="images/store.png" alt="" class="w-25">
            </div>
        </div>
        <div class="col-md-6 col-sm-12 col-xs-12">
            <img src="images/computer.png" alt="" class="w-75">
        </div>
    </div>
</div>
<div class="middle-content">
    <div class="row">
        <div class="col-md-6 col-sm-12 col-xs-12">
            <img src="images/Management.png" alt="" class="">
            <h3 class="text-uppercase">Gestion</h3>
            <p>Faciliter de bien gérer l’activité économique d’une entreprise</p>
        </div>
        <div class="col-md-6 col-sm-12 col-xs-12">
            <img src="images/Union.png" alt="" class="">
            <h3 class="text-uppercase">Facturation</h3>
            <p>Etablir une note détaillée des prestations ou marchandise  vendues</p>
        </div>
    </div>
</div>
<div class="last-section">
    <h2>GEREZ VOTRE ENTREPRISE AVEC FEWMU</h2>
    <div class="telec">
        <p>Téléchargez</p>
    </div>
</div>

@endsection
