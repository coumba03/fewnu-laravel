<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-Zenh87qX5JnK2Jl0vWa8Ck2rdkQ2Bzep5IDxbcnCeuOxjzrPF/et3URy9Bv1WTRi" crossorigin="anonymous">
        <link rel="stylesheet" href="{{asset('/assets/css/main.css')}}">

        <title>@yield('title', env('APP_NAME'))</title>  
    </head>
    <body>
        <nav class="navbar navbar-expand-lg bg-white p-0">
            <div class="container-fluid">
                <a class="navbar-brand" href="/accueil">
                    <img src="images/fewnu.png" alt="fewnu" class="w-75">
                </a>
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                  <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse mx-5 " id="navbarNav">
                    <ul class="navbar-nav  ms-auto">
                       <li class="nav-item me-5 ">
                          <a class="nav-link active" aria-current="page" href="/accueil">Accueil</a>
                       </li>
                       <li class="nav-item me-5">
                          <a class="nav-link" href="/a-propos">A propos</a>
                       </li>
                       <li class="nav-item me-5">
                          <a class="nav-link" href="/fonctionnalite">Fonctionnalités</a>
                       </li>
                       <li class="nav-item me-5">
                          <a class="nav-link" href="/download">Télécharger</a>
                       </li>
                       <li class="nav-item me-5 ps-5">
                          <a class="nav-link" href="/contact">Contacts</a>
                       </li>
                    </ul>
                </div>
            </div>
        </nav>  

        <div class="container-fluid">@yield('content')</div>

        <footer class="container-fluid">
            <div class="row">
                <div class="col-md-9 col-sm-12 col-xs-12 d-flex">
                    <div class="fewnu-b">
                        <img src="images/fewnu-b.png" alt="" class="">
                    </div>
                    <div class="text-disp">
                        <p>Nous vous coachons et vous aidons à atteindre vos objectifs pour votre <br> Business. Avec Fewnu, nous pouvons vous aider à chaque étape du processus. <br> Téléchargez votre Logiciel de Gestion Fewnu et facilitez votre quotidien. <br> Qu’est-ce que vous attendez pour faire le premier pas aujourd’hui ?
                        </p>
                    </div>
                </div>
                <div class="col-md-3 col-sm-12 col-xs-12 bg-info">
                    <img src="images/fcb.png" alt="" class="">
                    <img src="images/insta.png" alt="" class="">
                    <img src="images/twitt.png" alt="" class="">
                </div>
            </div>
            <hr>
            <div class="foot d-flex">
                <p>78 293 36 56</p>
                <p>Hlm Grand Yoff</p>
                <p>8:00 - 18:00</p>
            </div>
            <div class="copyri">
                <p>&copy; Copyright Fewnu – Logiciel {{date('Y')}} Tous Droits Réservés
                </p>
            </div>
        </footer>
        
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-OERcA2EqjJCMA+/3y+gxIOqMEjwtxJY7qPCqsdltbNJuaOe923+mo//f6V8Qbsw3" crossorigin="anonymous"></script>
    </body>
</html>
